// get the title of the page/Document
console.log(document.title);

// to get url of current page / document

console.log(document.URL);

// to get the complet body of the document object or webpage
console.log(document.body);

// to get the head of the document object
console.log(document.head);

// There are different ways to access the differents elements/node on webpage/documents
// 1. getElementById
// 2. getElementByClassName
// 3. getElementByTagName

// Recently there is another way of doing the same with
//1. querySelector("")  // This helps to get the first instance of the elemnt matched by value passed in quote. this is just a css selector we are using in JS
document.getElementsByClassName("header");

document.getElementsByTagName("span");

var addRemoveTag = document.querySelector(".add_remove");
var addHeader = document.createElement("h1");
var headText = document.createTextNode("Lets try adding h1");
addHeader.appendChild(headText);
console.log(addHeader);
addRemoveTag.appendChild(addHeader);
console.log(addRemoveTag);

const ADDREMOVEHEAD = document.querySelector(".add_remove h1");
var attributes = ADDREMOVEHEAD.attributes;
console.log(attributes);
ADDREMOVEHEAD.setAttribute("class","header");
console.log(attributes);

// Now lets create a function that gonna add <br> at the bottom of the body

var brTag = document.createElement("br");
console.log(brTag);
const lastDiv = document.querySelector(".add_remove");
console.log(lastDiv);
document.body.insertBefore(brTag,lastDiv);

lastDiv.parentNode.insertBefore(brTag, lastDiv.nextSibling); // to insert after any node

function addBr(){
  var brTag = document.createElement("br");
  console.log(brTag);
  const lastDiv = document.querySelector(".add_remove");
  console.log(lastDiv);
  document.body.insertBefore(brTag,lastDiv);
}

console.log(document.body.parentNode);

function insertAfter(newNode,referenceNode){
  //var referenceNode = document.querySelector(referenceNode);
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  console.log(referenceNode.parentNode);
}
//const lastDiv = document.querySelector(".add_remove");
var referenceNode= document.getElementsByTagName("br")[2];
console.log(referenceNode);
insertAfter(brTag, referenceNode);


// lets try adding image to our page at the bottom of the body
// Before this , lets try adding one more class to every h1 so we can have unique header

const  span = document.getElementsByTagName("span");
console.log(span);

const  H1 = document.getElementsByClassName("header")[0];
console.log(H1.className);
console.log(H1.classList);
H1.classList.add("header0");
console.log(H1.className);

for(var i=1 ; i<=9; i ++){
  var h1 = document.getElementsByClassName("header")[i];
  h1.classList.add("header"+i);
}

console.log(document.getElementsByClassName("header").length);

// Now lets try adding images under every heading section
// Adding js.png under first header

var imgCss = document.createElement("img");
imgCss.setAttribute("src", "resources/img/css.png");
imgCss.setAttribute("alt", "css logo");
imgCss.setAttribute("height", "80px");
imgCss.setAttribute("width", "80px");


var imgHtml = document.createElement("img");
imgHtml.setAttribute("src", "resources/img/html.png");
imgHtml.setAttribute("alt", "html logo");
imgHtml.setAttribute("height", "80px");
imgHtml.setAttribute("width", "80px");


var imgJs = document.createElement("img");
imgJs.setAttribute("src", "resources/img/js.png");
imgJs.setAttribute("alt", "js logo");
imgJs.setAttribute("height", "80px");



var head0 = document.getElementsByTagName("h1")[0];
console.log(head0);
head0.parentNode.insertBefore(imgJs, head0.nextSibling);

var head1 = document.getElementsByTagName("h1")[1];
head1.parentNode.insertBefore(imgCss, head1.nextSibling);

// insertAfter(imgCss, H1);

var head2 = document.getElementsByTagName("h1")[2];
head2.parentNode.insertBefore(imgHtml, head2.nextSibling);
