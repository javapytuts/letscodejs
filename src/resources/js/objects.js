
// JS is one of the oject oriented programming languagebut its quite sloppy as compared to other oop langages


// when we have cluster of related data and want to perform some oeperatios on it then its always good to create an object.

// Object are nothing but the cluster of related data and methods which can be used to perform operations on the data

// Object helps to combine the related properties and behavious

// In a more specific way, Objects are data model that allow us to combine specific dataset in a more stucture way.

// for example -- lets conside we have to define  a male

var name = "Andy";
var age = 45;
var address = "Seattle";

function studyJS(){
  console.log("Andy loves to study JS");
}

// Now in the above scenario we are trying to create Andy and his related properties and behaviour but it doesn't look interelated so what we gonna do we will make an object of the same

var andy = {
  name: "Andy",
  age: 45,
  address: "Seattle",
  course:"JS",
  studyJS: (function(){
    console.log("Andy loves to study " + this.course)
  })(),
  updateCourse: function(){
    this.course = "HTML/CSS";
    console.log("Andy wanna change to " + this.course);
  }
}

//now it look much meaningful and inter related since we have wrapped everything within object name andy

// mow lets access the properties of andy
console.log(andy.name);
console.log(andy.updateCourse());
console.log(andy.course);




var course = new Object();

course.title = "JavaScript";
course.instructor = "Rajeshwar S";
course.level = 1;
course.views = 0;


// Also we can do the above using

var course = {
  title : "JavaScript",
  instructor: "Rajeshwar Sharma",
  level: 1,
  views: 0,
  updateViews: function(){
    this.views = 1;
  }
}

// But int he above scenario there is restriction since we are hardcoding everything within object and hence when we have to define another course we have to go and and write whole lot of stuffs such as
var courseTwo = {
  title : "HTML/CSS",
  instructor: "Gaur nitai",
  level: 1,
  views: 20,
  updateViews: function(){
    this.views = ++this.views;
  }
}

// But this is not the efficient way and good practice since it just eads to the repetition of code and hence code become lengthy


// So to solve this limitations, we can use the CONSTRUCTORS
