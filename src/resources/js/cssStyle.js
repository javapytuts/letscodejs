
// lets try adding image to our page at the bottom of the body
// Before this , lets try adding one more class to every h1 so we can have unique header
var addRemoveTag = document.querySelector(".add_remove");
var addHeader = document.createElement("h1");
var headText = document.createTextNode("Lets try adding h1");
addHeader.appendChild(headText);
console.log(addHeader);
addRemoveTag.appendChild(addHeader);
console.log(addRemoveTag);

const ADDREMOVEHEAD = document.querySelector(".add_remove h1");
var attributes = ADDREMOVEHEAD.attributes;
console.log(attributes);
ADDREMOVEHEAD.setAttribute("class","header");
console.log(attributes);


const  span = document.getElementsByTagName("span");
console.log(span);

const  H1 = document.getElementsByClassName("header")[0];
console.log(H1.className);
console.log(H1.classList);
H1.classList.add("header0");
console.log(H1.className);

for(var i=1 ; i<=9; i ++){
  var h1 = document.getElementsByClassName("header")[i];
  h1.classList.add("header"+i);
}

console.log(document.getElementsByClassName("header").length);

// Now lets try adding images under every heading section
// Adding js.png under first header

var imgCss = document.createElement("img");
imgCss.setAttribute("src", "resources/img/css.png");
imgCss.setAttribute("alt", "css logo");
imgCss.setAttribute("height", "80px");
imgCss.setAttribute("width", "80px");


var imgHtml = document.createElement("img");
imgHtml.setAttribute("src", "resources/img/html.png");
imgHtml.setAttribute("alt", "html logo");
imgHtml.setAttribute("height", "80px");
imgHtml.setAttribute("width", "80px");


var imgJs = document.createElement("img");
imgJs.setAttribute("src", "resources/img/js.png");
imgJs.setAttribute("alt", "js logo");
imgJs.setAttribute("height", "80px");

var head0 = document.getElementsByTagName("h1")[0];
console.log(head0);
head0.parentNode.insertBefore(imgJs, head0.nextSibling);

var head1 = document.getElementsByTagName("h1")[1];
head1.parentNode.insertBefore(imgCss, head1.nextSibling);

// insertAfter(imgCss, H1);

var head2 = document.getElementsByTagName("h1")[2];
head2.parentNode.insertBefore(imgHtml, head2.nextSibling);

// Here we gonna discuss how we can get and set  style to the elements.
const HEADER4 = document.querySelector(".header4");

console.log(HEADER4.style);  // This will return onliy the inline cSS to that element
HEADER4.style.color = "#ff7c19";
console.log(HEADER4.currentStyle);
var h4Style = window.getComputedStyle(HEADER4);  // this will return all the style applied on h4 from internal and external files both and inline CSS
console.log(h4Style);
console.log(window.getComputedStyle);
console.log(h4Style.color);
console.log(h4Style['font-family']);
console.log(h4Style['font-style']);
console.log(h4Style['font-weight']);

HEADER4.style['font-style']='italic';
HEADER4.style.backgroundColor = '#42273B';
HEADER4.style['border-radius']= '5px';
// In the above scenario, we are setting the property one by one which is kind of tedious

// Lets set in one go and to do so we need to use cssText property

HEADER4.style.cssText += 'padding:5px; margin:5px; padding-right: 10px;';

// In the above example , we have used += which help to append the next properties with the existing one
//Also , if we observer closely, there is something interesting we can observe. Style which we are setting to the elements as inline CSS acts as an attribute of the element and hence we can use even attributes, getAttribute(), hasAttribute(), setAttribute() function to perform different operations
// Here is an example
console.log(HEADER4.attributes);
console.log(HEADER4.setAttribute('style','font-style:normal; font-weight:900;'));
//
