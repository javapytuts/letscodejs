// Whenever we have to perform operations with different values its a lways good to have function with paramater
// Functions are nothing but a small block of code which helps to perform specific operations
// Function describes the behaviour of any object
// There are 3 types of functions in JS - Named functions / Anonymous Functions / Immediately Invoked Fucntion Expression(IIFE)


// Lets discuss somethin about Named functions
// Named sunction has its own name and can be used whenever we have to use/call function at multiple time with different values.
function named(){
  console.log("I m a named function");
}


function add(a, b){
  console.log("I m a named function with name - add");
  var sum = a + b;
  return sum;
}



// Anonymous function are those functions which are not having any name
var anonymous = function(){
  console.log("I m anonymous function");
}

anonymous();

var add = function(a, b){
  console.log("I m anonymous function");
  var sum = a + b;
  return sum;
}

var sum = add(20, 30);
console.log(sum);

// Immediately Invoked Function Expression

// Such function which are anonymous and having double parenthesis at the end of the function
(function(){
  console.log("I m IIFE");
})();


// When to use IIFE - when we have to call anonymous frunction at the time of loading html we can use IIFE

var anonymous_iife = (function(a, b){
  console.log("I m anonymous function");
  var sum = a + b;
  return sum;
})(20, 50);

console.log(anonymous_iife);


// FROM the above example codes we acn see, all the functions can be parameterized and invoked with arguments
// Always remeber whenever we are defining any IIFE function make sure we need to define the variable before it invoked
