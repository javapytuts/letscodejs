// closures are nothing but the fucntion inside function which are completely rely on outer function local variables


// before discussing more about closures lets consider first one simple scenario

function closureExample(){
  var a = 67;
  var b = 56;

  var sum = a + b;
  return sum;
}

console.log(closureExample());


// So here if we try to access a / b / sum it will throw error or return Undefined since we are trying to access the local variable of the function

// But if we still try to access those then we can just use the concept of closures as follows:

function closureExampleTwo(){
  var a = 60;
  var b = 70;

  function add(){
    var sum = a + b;
    return sum;
  }

  return add;
}

console.log(closureExampleTwo);
var closureFunct = closureExampleTwo();
console.log(closureFunct);
console.log(closureFunct());
