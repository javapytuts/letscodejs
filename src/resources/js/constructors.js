// Consider the smae course example that we have discussed earlier

// lets create constructor for course

function Course(title, instructor, views){
  this.title = title;
  this.instructor = instructor;
  this.views = views;
  this.updateViews = function(){
    console.log("Current course views " + this.views);
    ++this.views;
    return this.views;
  }
}



// Now lets create different course objects from this constructors;

var javascript = new Course("JavaScript Essential", "Rajeshwar Sharma", 1);
var htmlCss = new Course("HTML/CSS" , "Gaur Nitai", 200);

console.log(javascript);
console.log(htmlCss);
console.log(javascript.title);
console.log(htmlCss.updateViews());
