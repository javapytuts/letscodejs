// there are 2 types of variable mostly used in JS
// local variable & global variable

(function(){
  var x = "I m local variable";
  console.log(x);
  console.log(globalVariable);
})();

var globalVariable= 123;
var globalVariableTwo= "This is a global variable";


// two new types of variable introduced in ECMASCript2015/ES6

//const & let

//const - Can't be change once defined since its the global/universal constant
// let - helps to define the scoping of variable at more granular level
// let define at block level  block level variable
// let having smaller scope then var

const PI = 3.14;
console.log(3.14);

console.log(PI);

// try{
//   PI = 67; // conatnat cannot be changed in JS and will throw error
// } catch(exception e){
//   console.log("Cant assign value to constant variable");
// }

// Now lets check the usage of let

function add(){
  var a = 10;
  var b = 56;
  if(a > 5){
    var b = 50;  // this will change the scope of global variable too though we are trying to define new local variable. this will treated as same as global variable
    console.log(b);
  }
console.log(b);
}

add();

// So differentiate both local a & global a we should use the concept of let

function mul(){
  var a = 10;
  var b = 56;
  if(a > 5){
    let b = 50;  // this will change the scope of global variable too though we are trying to define new local variable. this will treated as same as global variable
    console.log(b);
  }
console.log(b);
}

mul();

// Also we can do

function div(){
  var a = 10;
  let b = 56;
  if(a > 5){
    let b = 50;  // this will change the scope of global variable too though we are trying to define new local variable. this will treated as same as global variable
    console.log(b);
  }
console.log(b);
}

div();
